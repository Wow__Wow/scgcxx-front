const $copyObject = function(srcObj){
	let newObj = {};
	for(let prop in srcObj){
		newObj[prop]=srcObj[prop];
	}
	return newObj;
}


const $axios = axios.create({
	baseURL: 'http://localhost/',
	withCredentials: true//向服务器发送认证信息（session）
});

if (Vue) {

	Vue.prototype.$axios = $axios;//为vue设置原型属性$axios

	Vue.prototype.$ajax = function (getAxiosPromise,loadingText) {



		let promise = new Promise((resolve, reject) => {

			try {

				const loading = this.$loading({
					lock: true,
					text: loadingText?loadingText:'加载中......',
					background: 'rgba(200, 200, 200, 0.5)'
				});

				getAxiosPromise()
					.then(res => {

						loading.close();

						let result = res.data;

						if (!result.hasOwnProperty('logined')) {
							resolve(result);
							return;
						}

						if (!result.logined) {
							this.$message({
								message: '您已下线，请登录！',
								type: 'error'
							});
							window.setTimeout(()=>{window.top.location.href='../../safty/login/index.html';},1500);
							return;
						}

						resolve(result);

					})
					.catch(err => {
						loading.close();
						console.log(err);
						this.$message({
							message: `错误代码：${err.response.status}`,
							type: 'error'
						});
					});

			} catch (error) {
				console.log(error);
				this.$message({
					message: `JavaScript的代码错误！`,
					type: 'error'
				});
				throw e;
			}

		});

		return promise;

	};

	Vue.prototype.$myoprate = function (getAxiosPromise, confirmInfo) {

		let promise = new Promise((resolve,reject)=>{

			if (confirmInfo && confirmInfo.message && confirmInfo.title) {

				this.$confirm(confirmInfo.message, confirmInfo.title, {
					confirmButtonText: '确定',
					cancelButtonText: '取消',
					type: 'warning'
				}).then(() => {
					this.$ajax(getAxiosPromise,'执行中，请等待.....')
						.then((result) => {
	
							if (result.success) {								
								this.$message({
									message: result.message,
									type: 'success'
								});
								resolve(result);
							} else {
								this.$alert(result.message, {
									type:'error',
									confirmButtonText: '确定'
								});
							}
	
						});
				}).catch(()=>{
					this.$message({
						message: '操作已取消！',
						type: 'info',
						duration:800
					});
				});
	
			} else {
				this.$ajax(getAxiosPromise)
					.then((result) => {

						if (result.success) {
							this.$message({
								message: result.message,
								type: 'success'
							});
							resolve(result);
						} else {
							this.$alert(result.message, {
								type:'error',
								confirmButtonText: '确定'
							});
						}

					});

			}

		});

		return promise;

	}

	Vue.prototype.$myGet = function (url, config) {

		let promise = new Promise((resolve, reject) => {

			this.$ajax(() => $axios.get(url, config))
				.then((result) => {
					resolve(result);
				});

		});

		return promise;
	};


	Vue.prototype.$myPost = function (url, data, config) {		

		return this.$myoprate(() =>{return $axios.post(url, data, config);} ,config);
		
	};

	Vue.prototype.$myPut = function (url, data, config) {		

		return this.$myoprate(() =>{return $axios.put(url, data, config);} ,config);
		
	};

	Vue.prototype.$myDelete = function (url,config) {		

		return this.$myoprate(() =>{return $axios.delete(url, config);} ,config);
		
	};




}